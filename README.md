# ag_assessment_3

# Installation
- You should have are installed Node.js development environment and Yarn.
- Clone the repository by using `git clone git@gitlab.com:zeeebr/ag_assessment_3.git`.
- Run `yarn` in the cloned directory.

- Add variables to your environment (`nano .env` in the cloned directory):

```
NODE_ENV={{YOUR_ENVIRONMENT}}
HTTP_PORT={{YOUR_APP_HTTP_PORT}}
HEADLESS=true
```

# How to Run
```
node index.js
```

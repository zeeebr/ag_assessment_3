import listParser from './parsers/list.parser.js'
import randomTimeout from './utils/random.timeout.js'
import itemParser from './parsers/item.parser.js'
import * as fs from 'fs'
import imageSaver from './parsers/image.saver.js'

const firstPage = 2
const lastPage = 10

index()

async function index() {
  console.log('App started!')

  if (!fs.existsSync('data')) {
    fs.mkdirSync('data')
    console.log(`Folder 'data' created!`)
  } else {
    console.log(`Folder 'data' exist!`)
  }

  const linksSet = new Set()

  for (let page = firstPage; page <= lastPage; page++) {
    try {
      console.log(`Processing page #${page}...`)

      const extractedLinks = await listParser(page)

      extractedLinks.forEach(link => linksSet.add(link))

      console.log(`Extracted ${extractedLinks.length} from page #${page}!`)

      if (page !== lastPage) {
        await randomTimeout(7, 12)
      }
    } catch (err) {
      console.error(`Error with page: ${page}, error message: ${err.message}`)
    }
  }

  console.log(`Extracted ${linksSet.size} unique links from ${lastPage - firstPage + 1} pages!`)

  for (const link of linksSet) {
    try {
      const item = await itemParser(link)

      const { data, images } = item
      const { id } = data

      if (!fs.existsSync(`data/${id}`)) {
        await fs.promises.mkdir(`data/${id}`)
        console.log(`Folder 'data/${id}' created!`)

        const jsonString = JSON.stringify(data, null, 2)

        const filePath = `data/${id}/info.txt`

        await fs.writeFile(filePath, jsonString, (err) => {
          if (err) {
            console.error('Error with saving txt file:', err)
          } else {
            console.log(`Data saved in: ${filePath}`)
          }
        })

        for (let number = 1; number < images.length; number++) {
          const url = images[number]
          await imageSaver(url, `data/${id}`, number)
          console.log(`Image ${number} saved!`)
          await randomTimeout(1, 2)
        }
      } else {
        console.log(`Folder 'data/${id}' exist!`)
      }

      await randomTimeout(7, 12)
    } catch (err) {
      console.error(err)
    }
  }
}
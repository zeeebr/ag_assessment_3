import axios from 'axios'
import * as fs from 'fs/promises'

export default async function imageSaver(url, path, number) {
  try {
    const response = await axios.get(url, { responseType: 'arraybuffer' })
    const imageData = Buffer.from(response.data, 'binary')
    const fileName = `${path}/image_${number}.jpg`
    await fs.writeFile(fileName, imageData)
  } catch (error) {
    console.error(`Error with image ${number}:`, error.message)
  }
}

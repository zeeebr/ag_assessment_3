import axios from 'axios'
import * as cheerio from 'cheerio'

export default async function itemParser(url) {
  const { data: html } = await axios.get(url)

  const $ = cheerio.load(html)

  const nextData = $('script[id="__NEXT_DATA__"]').text()

  const json = JSON.parse(nextData)

  const { props } = json
  const { pageProps } = props
  const { dehydratedState, statementId } = pageProps
  const { queries } = dehydratedState

  const id = parseInt(statementId)

  const match = queries.find(item => item.state.data.data.statement.id === id)

  const { statement } = match.state.data.data

  const { dynamic_title, address, price, floor, area, total_floors, room_type_id, bedroom_type_id, gallery } = statement

  const images = gallery.map(item => item.image.large)

  const priceUSD = price['2'].price_total

  const item = {
    data: {
      id,
      flatFloor: floor,
      totalFloors: total_floors,
      bedrooms: bedroom_type_id,
      rooms: room_type_id,
      area,
      priceUSD,
      description: dynamic_title,
      address
    },
    images
  }

  return item
}
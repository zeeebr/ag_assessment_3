import puppeteer from 'puppeteer-extra'
import StealthPlugin from 'puppeteer-extra-plugin-stealth'
import AdblockerPlugin from 'puppeteer-extra-plugin-adblocker'
import env from '../utils/env.js'

export default async function listParser(page) {
  let browser

  const url = `https://www.myhome.ge/en/s/iyideba-bina-Tbilisshi/?deal_types=1&real_estate_typ es=1&cities=1&currency_id=1&CardView=2&urbans=47&districts=4&page=${page}`

  try {
    puppeteer.use(AdblockerPlugin())
    puppeteer.use(StealthPlugin())

    browser = await puppeteer.launch({
      headless: (env.HEADLESS) ? 'new' : false,
      args: ['--no-sandbox', '--disable-setuid-sandbox']
    })

    const page = await browser.newPage()

    await page.goto(url, { waitUntil: 'networkidle0' })

    const links = await page.evaluate(() => {
      const anchors = Array.from(document.querySelectorAll('a[href^="/en/pr/"]'))
      return anchors.map(anchor => anchor.href)
    })

    return links
  } finally {
    if (browser) {
      await browser.close()
    }
  }
}
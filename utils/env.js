import { cleanEnv, str, bool } from 'envalid'
import 'dotenv/config'

const env = cleanEnv(process.env, {
  NODE_ENV: str({ choices: ['development', 'production'] }),
  HEADLESS: bool({ default: false }),
  USER_AGENT: str()
})

export default env

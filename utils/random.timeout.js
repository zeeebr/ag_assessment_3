import { setTimeout } from 'node:timers/promises'

export default async function randomTimeout(min, max) {
  const minMs = min * 1000
  const maxMs = max * 1000
  const timeout = Math.floor(Math.random() * (maxMs - minMs + 1)) + minMs

  console.log(`Timeout ${timeout/1000} secs...`)

  await setTimeout(timeout)
}